package com.demo.blog.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.blog.model.Blog;
import com.jfinal.plugin.activerecord.Page;

/**
 * blog服务
 * @author lich
 *
 */
@Service
@Transactional
public class BlogService {
	
	/**
	 * 所有 sql 与业务逻辑写在 Model 或 Service 中，不要写在 Controller 中，养成好习惯，有利于大型项目的开发与维护
	 */
	public Page<Blog> paginate(int pageNumber, int pageSize) {
		return Blog.me.paginate(pageNumber, pageSize, "select *", "from blog order by id asc");
	}
	
	public void update(Blog blog){
		blog.update();
	}
	

	@Transactional(noRollbackFor=RuntimeException.class)
	public void save(Blog blog){
			blog.save();
			throw new RuntimeException("sdf");
	}
	
	public Blog findById(Integer paraToInt) {
		return Blog.me.findById(paraToInt);
	}

	public void deleteById(Integer paraToInt) {
		Blog.me.deleteById(paraToInt);
	}
}
